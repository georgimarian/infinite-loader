import React, {useState, useEffect, useCallback, useMemo, useRef} from 'react';

import Beer from './model/Beer';
import useInfiniteLoader from './hooks/useInfiniteLoader';
import './App.css';

const URL = 'https://api.openbrewerydb.org/breweries?';
const ELEMENTS_PER_PAGE = 10;

/*
  Mock fetcher used to test API running out of objects to fetch
*/
const mockArray = new Array(55).fill(null).map((_, index) => ({id: `id_${index}`, name: `name_${index}`}));
const loadPageLimited = (page: number) => new Promise(
  (resolve) => setTimeout(
    () => resolve(mockArray.slice((page - 1) * ELEMENTS_PER_PAGE, page * ELEMENTS_PER_PAGE)),
    2000
    )
  );

const App = () => {
  const loadPage = (page: number) =>
    fetch(`${URL}page=${page}&per_page=${ELEMENTS_PER_PAGE}`)
      .then(response => response.json());

  const {lastElementRef, isLoading, isDone, data: beers} = useInfiniteLoader<Beer>(loadPage, ELEMENTS_PER_PAGE, null);

  return (
    <div className="App">
      <ul className="container">
        {beers.map((beer, index) => (
          <div
            key={beer.id}
            className="card-container"
            ref={(index === beers.length -1 && !isDone)
              ? lastElementRef
              : undefined
            }
          >
            <p>{beer.name}</p>
          </div>
        ))}
        {isLoading && <p>...Loading...</p>}
      </ul>
    </div>
  );
}

export default App;
