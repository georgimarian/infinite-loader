interface Beer{
  id: string,
  name: string,
  brewery_type: string,
  street: string,
  city: string,
  state: string
};

export default Beer;
