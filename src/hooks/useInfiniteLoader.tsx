import React, {useState, useEffect, useCallback, useMemo, useRef} from 'react';

function useInfiniteLoader<Model>(
  fetcher: (page: number) => Promise<Model[]>, pageSize: number, root: HTMLElement | null
) {
  const lastElementRef = useRef<HTMLDivElement>(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isDone, setIsDone] = useState(false);
  const [page, setPage] = useState(1);
  const [data, setData] = useState<Model[] | []>([]);

  const handleIntersect = useCallback(
    (entries: IntersectionObserverEntry[], observer: IntersectionObserver) =>
      entries.forEach((entry)=> {
        if (entry.isIntersecting && lastElementRef.current) {
          setPage(prevPage => prevPage + 1);
          observer.unobserve(lastElementRef.current);
        }
      }
    ),
    [setPage]
  );

  const observer = useMemo(
    () => new IntersectionObserver(handleIntersect, { root: root}),
    [handleIntersect]
  );

  useEffect(() => {
    if(lastElementRef.current)
      observer.observe(lastElementRef.current);
  }, [data]);

  useEffect(()=> {
    setIsLoading(true);
    fetcher(page)
      .then(nextData => {
        setIsDone(nextData.length < pageSize);
        setData(prevData => [...prevData, ...nextData]);
      })
      .finally(() => setIsLoading(false))
  }, [page]);

  return {lastElementRef, isLoading, isDone, data};
}

export default useInfiniteLoader;
