# React Infinite Loader

This project showcases a React Infinite Loader component.

## Infinite loader

This application provides an implementation for a generic infinite loader.

## Assumptions

An endpoint from [Open Brewery DB]( https://www.openbrewerydb.org/documentation) was used. The endpoint uses query params "page=" for the page number and "per_page=" for page size.
A timeout of 2s was used as the API response was very fast.

**Note: this was done in order to skip mocking data**
